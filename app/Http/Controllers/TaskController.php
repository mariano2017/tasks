<?php

namespace App\Http\Controllers;

use App\Entities\Task;
use App\Http\Requests\TaskRequest;
use App\Repositories\Repository;

class TaskController extends Controller
{
    protected $model;

    public function __construct(Task $task)
    {
        $this->model = new Repository($task);
    }

    public function index()
    {
        return $this->model->all();
    }

    public function create(TaskRequest $request)
    {

        return ($this->model->create($request->all())) ?
            redirect()->back()->with(['status'=>'Tarefa criada com sucesso!']) :
            redirect()->back()->with(['status'=>'Erro ao cadastrar']);

    }

    public function read($id)
    {
        return $this->model->find($id);
    }

    public function update(TaskRequest $request, $id)
    {
        return ($this->model->update($request->all(), $id)) ?
            redirect()->back()->with('status', 'Tarefa atualizada com sucesso!') :
            redirect()->back()->with('status', 'Erro ao atualizar!');
    }

    public function delete($id)
    {
        return ($this->model->delete($id)) ?
            redirect()->back()->with('status', 'Tarefa deletada com sucesso!') :
            redirect()->back()->with('status', 'Erro ao deletar!');
    }
}
