<?php

namespace App\Services;

use App\Repositories\Repository;
use Illuminate\Http\Request;

class Service
{
    protected $model;

    public function __construct(Repository $model)
    {
        $this->model = $model ;
	}

    public function index()
    {
        return $this->model->all();
	}

    public function create(Request $request)
    {
        $attributes = $request->all();
         return $this->model->create($attributes);
    }

    public function read($id)
    {
        return $this->model->find($id);
    }

    public function update(Request $request, $id)
    {
        $attributes = $request->all();
        return $this->model->update($id, $attributes);
    }

    public function delete($id)
    {
        return $this->model->delete($id);
    }
}
