## Exemplo de API em Laravel

####Configuração

- Caso não possua o XAMPP instalado, baixar-se o xampp.exe e instalar-se;
- Caso não possua o Composer instalado, baixar-se o Composer.exe e instalar-se;
- Abrir-se um terminal e executar-se o comando composer;
- Navegar-se até a pasta do projeto e executar-se o comando composer update no terminal;
- Executar-se o comando no terminal: copy .env.example .env
- Criar-se um banco de dados MySql com o nome tasks;
- Configurar-se o arquivo .env do projeto para apontar as seguintes configurações do banco recém criado:

	- DB_DATABASE={"Nome do Banco de Dados"}
	- DB_USERNAME={"Usuário do Banco de Dados"}
	- DB_PASSWORD={"Senha do Banco de Dados"}

- Depois disso, executar-se o comando no terminal: php artisan migrate
- Após isso, executar-se o comando no terminal: php artisan serve

####Uso da Api

Esse é um exemplo simples de um crud de tarefas com Laravel, usando o Pattern Repository como padrão. Aqui são os links para interagir com a api: 

- Link da API : http://127.0.0.1:8000/;
- GET : api/tasks/task;
- GET : api/tasks/task/{id};
- POST : api/tasks/task;
- PUT : api/tasks/task/{id};
- DELETE : api/tasks/task/{id}.

Obs : Se estiver usando um programa como o POSTMAN, colocar no corpo da mensagem para cadastro e atualização, um json com os seguintes parâmetros: {"id" : "", "title" : "{um titulo qualquer}", "body" : "{um texto qualquer}"}.

